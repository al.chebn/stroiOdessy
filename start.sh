#!/bin/sh
systemctl stop apache.service
cd ~/project/stroiOdessy/ && git pull origin master && cd -
source ~/project/stroiOdessy/venv/bin/activate
fuser -k -n tcp 80
python ~/project/stroiOdessy/manage.py runserver 0.0.0.0:80