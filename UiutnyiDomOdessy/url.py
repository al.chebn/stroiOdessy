from django.urls import path
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('', views.main),
    path('index/', views.main),
    path('advantage/', views.advantage),

    path('partner/<str:name_company>/', views.partner),

    path('gallery/', views.gallery),
    path('gallery/<str:dom>/', views.gallery),

    path('contact/', views.contact),
    path('score/', views.score),
    path('about/', views.about),

    path('robots.txt/', views.robot),
    path('crossdomain.xml/', views.crossdomain),

    path('admin/', views.admin),
    path('admin/attendance', views.attendance),  # посещаемость
    path('admin/review', views.review),  # номера клиентов
    path('admin/form', views.form)  # Заполненные формы - relation
]
