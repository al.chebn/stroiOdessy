from django import forms


class ContactUser(forms.Form):
    emailPhone = forms.CharField()  # email or phone
