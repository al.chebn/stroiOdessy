from pymongo import MongoClient
from bson.objectid import ObjectId


class DataBaseEmail:
    client = MongoClient('localhost', 27017)
    db = client.UiutnyiDomOdessy

    def insert(self, email):
        collection = self.db.email
        document = {'email': email}
        insert = collection.insert(document)
        return ObjectId(insert)

    def get(self):
        collection = self.db.email
        list_email = []
        for i in collection.find():
            list_email.append(i['email'])
        return list_email

    def insert_form(self, name, email, theme, message):
        collection = self.db.form
        document = {'name': name, 'email': email, 'theme': theme, 'message': message}
        insert = collection.insert(document)
        return ObjectId(insert)
        pass

    def get_form(self):
        collection = self.db.form
        list_form = []
        for i in collection.find():
            list_form.append([i['name'], i['email'], i['theme'], i['message']])
        return list_form
