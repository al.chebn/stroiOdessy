import os

from django.shortcuts import render, render_to_response

from UiutnyiDomOdessy.models import DataBaseEmail
from .forms import ContactUser

# Create your views here.
contact_user = ContactUser()

db = DataBaseEmail()


def get_contact(request):
    if request.method == 'POST':
        if request.POST.get('user_email'):
            email_phone = request.POST.get('user_email')
            db.insert(email_phone)


def main(request):
    get_contact(request)
    return render(request, 'index.html')


def contact(request):
    if request.method == 'POST':
        if request.POST.get('user_email'):
            email_phone = request.POST.get('user_email')
            db.insert(email_phone)
        if request.POST.get('name') and request.POST.get('email'):
            name = request.POST.get('name')
            email = request.POST.get('email')
            theme = request.POST.get('subject')
            message = request.POST.get('message')
            db.insert_form(name, email, theme, message)
    return render(request, 'contact.html')


def advantage(request):
    get_contact(request)
    return render(request, 'advantage.html')


def gallery(request, dom=None):
    get_contact(request)
    if dom:
        path = f'{os.getcwd()}/UiutnyiDomOdessy/static/images/galleryImages/{dom}'
        quantity = os.listdir(path)
        return render(request, 'iterGallery.html', {'iter_picture': quantity, 'name': dom})
    else:
        return render(request, 'gallery.html')


def score(request):
    get_contact(request)
    return render(request, 'score.html')


def partner(request, name_company):
    get_contact(request)
    number_photo = os.listdir(os.getcwd() + '/UiutnyiDomOdessy/static/images/partnerImages/' + name_company)
    return render(request, 'partner.html',
                  {'name_logo': name_company.title(), 'name': name_company, 'number': number_photo})


def about(request):
    get_contact(request)
    return render(request, 'about.html')


def robot(request):
    return render(request, 'robots.txt', content_type="text/plain")

def crossdomain(request):
    return render(request, 'crossdomain.xml', content_type="text/xml")

def admin(request):
    return render(request, 'base_admin.html')


def attendance(request):
    return render(request, 'templateAdmin/attendance.html')


def review(request):
    list_contact = db.get()  # list email or number phone
    return render(request, 'templateAdmin/review.html', {'contact': list_contact})


def form(request):
    list_data_form = db.get_form()
    return render(request, 'templateAdmin/form.html', {'data': list_data_form})
