from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('UiutnyiDomOdessy.url')),
    path('admin/', admin.site.urls),
]
